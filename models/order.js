const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	

	userId:{
		type: "String"
	},
	email:{
		type: "String"
	},
	quantity:{
		type: Number,
	},
	totalAmount:{
		type: "String"
	},
	productId:{
		type: "String"
	},
	image:{
		type: "String"
	},
	name:{
		type: "String"
	},

	purchasedOn:{
		type: Date,
		default: new Date()
	}

});


module.exports = mongoose.model("Order", orderSchema);