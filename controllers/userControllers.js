const User = require("../models/user.js");
const Product = require("../models/product.js");
const Order = require("../models/order.js");
const Cart = require("../models/cart.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// User registration
module.exports.registerUser = (req, res) =>{

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	console.log(newUser);

	return newUser.save()
	.then(user => {
		console.log(user);
		res.send(true);
	})
	.catch(error =>{
		console.log(error);
		res.send("Invalid Registration");
	})
}


// User authentication
module.exports.loginUser = (req, res) =>{
	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			return res.send(false);
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){
				console.log("User Successfully Login")
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else{
				return res.send(false);
			}
		}
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	});
}




module.exports.getProfile = (reqBody) => {

	return User.findById({_id: reqBody.id}).then((result,err) => {

		if(err){
			return false;
		}
		else{
			result.password = "******";
			return result.save()
		}
		
	})

}

// Retrive User Details (Admin)
module.exports.getUser = (req, res) => {

	return User.find({email: req.body.email}).then(result => {
		result.password = "******";
		res.send(result);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	});
}


// Retrive User Details
module.exports.getUserProfile = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "******";
		res.send(result);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	});
}

// Check email exist
module.exports.checkEmailExists = (req, res) =>{
	return User.find({email: req.body.email}).then(result =>{

		console.log(result);

		// The user already exists
		if(result.length > 0){
			return res.send(true);
			// return res.send("User already exists!");
		}
		// There are no duplicate found.
		else{
			return	res.send(false);
			// return res.send("No duplicate found!");
		}
	})
	.catch(error => res.send(error));
}

// [STRETCH GOAL]
// Set user as admin (Admin Only)
module.exports.updateUserToAdmin = (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let updateActiveField = {
		isAdmin: req.body.isAdmin
	}

	if(userData.isAdmin === true){
	return User.findByIdAndUpdate(req.params.userId, updateActiveField, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
	}
	else{
		return res.send("User must be ADMIN to access this functionality!")
	}
}


// Retrieve authenticated user's orders
module.exports.getUserOrders = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let newData = {
		userId: userData.id
	}

	const orderData = Order.find({userId: newData.userId},{__v: 0, purchasedOn: 0}).then(order => {
		console.log(order)
		return res.send(order);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	});
}

// Retrieve authenticated user's orders
module.exports.getUserOrders = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let newData = {
		userId: userData.id
	}

	const orderData = Order.find({userId: newData.userId},{__v: 0, purchasedOn: 0}).then(order => {
		console.log(order)
		return res.send(order);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	});
}



// Retrieve all orders (Admin only)
module.exports.getAllOrders = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true){
		Order.find({},{__v: 0, purchasedOn: 0}).then(orders => res.send(orders)).catch(error => res.send(error));
	}
	else{
		return res.send("User must be ADMIN to access this functionality!")
	}
}



// Retrieve all User Details (Admin Only)
module.exports.getAllUserDetails = (req, res) => {

	const userData = auth.decode(req.headers.authorization); 

	if(userData.isAdmin == true){
		return User.find({},{__v: 0}).then(users => res.send(users)).catch(error => res.send(error));
	}
	else{
		return res.send(false)
	}
};

// Delete User
module.exports.deleteUser = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true){
		return User.findByIdAndDelete(req.params.userId)
		.then(result => {
			console.log(result);
			res.send("User has been deleted");
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
	}
	else{
		return res.status(404).send("Ianccessible page, admin permission needed!");
	}

}
