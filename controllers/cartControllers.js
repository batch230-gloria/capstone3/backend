const mongoose = require("mongoose");
const User = require("../models/user.js");
const Product = require("../models/product");
const Order = require("../models/order.js");
const Cart = require("../models/cart.js");
const auth = require("../auth");


// Add to Cart
// module.exports.addToCart = (req, res) => {

//  	const userData = auth.decode(req.headers.authorization);

//  	let newCart = new Cart({
// 		userId: userData.id,
// 		productId: {},
// 		price: {},
// 		quantity: req.body.quantity,
// 		subTotal:{}
// 	});

//  	let productData = Product.findById(req.params.productId)
//  	.then(product =>{	
// 		newCart.subTotal = product.price * newCart.quantity
// 		newCart.productId = (req.params.productId)	
// 		newCart.price = (product.price)
// 		newCart.save()
// 		return res.send("Product added to cart")
// 	})
// 	.catch(error =>{
// 		console.log(error)
// 		res.send("404 error")
// 	})

// };

module.exports.addToCart = (req, res) => {

 	const userData = auth.decode(req.headers.authorization);

 	let newCart = new Cart([])


 	let cart = {
		userId: userData.id,
		productId: {},
		price: {},
		quantity: req.body.quantity,
		subTotal:{}
	};

 	let productData = Product.findById(req.params.productId)
 	.then(product =>{	
		cart.subTotal = product.price * cart.quantity
		cart.productId = (req.params.productId)	
		cart.price = (product.price)

		newCart.push(cart)

		newCart.save()
		return res.send("Product added to cart")
	})
	.catch(error =>{
		console.log(error)
		res.send("404 error")
	})

};



// Retrieve all items in cart
module.exports.getUserCart = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let newData = {
		userId: userData.id
	}

	const cartData = Cart.find({userId: newData.userId},{__v: 0, purchasedOn: 0})
	.then(cart => {
		console.log(cart)
		return res.send(cart);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	});
}

// Update Cart
module.exports.updateCart =  (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	
	let updateCart = {
		quantity: req.body.quantity,
		subTotal: {}

	}

	let cartData =  Cart.findById(req.params.cartId)
	.then(cart =>{

		cart.quantity = updateCart.quantity
		updateCart.subTotal = cart.price * updateCart.quantity 
		cart.subTotal = updateCart.subTotal
		cart.save()
		res.send(cart);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	});

	
}



