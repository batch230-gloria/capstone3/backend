const express = require("express");
const router = express.Router();
const cartControllers = require("../controllers/cartControllers.js");
const auth = require("../auth.js")


router.post("/:productId/addToCart", auth.verify, cartControllers.addToCart);

router.patch("/:cartId", auth.verify, cartControllers.updateCart);

router.get("/myCart", auth.verify, cartControllers.getUserCart);


module.exports = router;