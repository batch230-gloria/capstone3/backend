const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers.js");
const auth = require("../auth.js");

router.post("/:productId/makeOrder", auth.verify, orderControllers.addOrder);

router.delete("/delete/:orderId", auth.verify, orderControllers.deleteOrder);

router.post("/checkOut", auth.verify, orderControllers.checkOutCart);

module.exports = router;