const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");


router.post("/add", auth.verify, productControllers.addProduct);

router.get("/all", productControllers.getAllProduct);

router.get("/active", productControllers.getAllActiveProduct);

router.get("/:productId", productControllers.getProduct);

router.patch("/update/:productId", auth.verify, productControllers.updateProduct);

router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

router.delete("/delete/:productId", auth.verify, productControllers.deleteProduct);

module.exports = router;