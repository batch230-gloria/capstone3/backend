const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");


router.post("/register", userControllers.registerUser);

router.post("/checkEmail", userControllers.checkEmailExists);

router.post("/login", userControllers.loginUser);

router.get("/profile", userControllers.getUser);

router.get("/details", auth.verify, userControllers.getUserProfile);

router.patch("/updateUser/:userId", userControllers.updateUserToAdmin); 

router.get("/orders", auth.verify, userControllers.getUserOrders);

router.get("/allOrders", auth.verify, userControllers.getAllOrders);

router.get("/allUsers", auth.verify, userControllers.getAllUserDetails);

router.delete("/delete/:userId", auth.verify, userControllers.deleteUser);

router.post("/userDetails", (request,response)=>{
	userControllers.getProfile(request.body)
	.then(resultFromController => response.send(resultFromController));
})


module.exports = router;
